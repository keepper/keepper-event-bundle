<?php
namespace Keepper\KeepperEventBundle\Services;

use Symfony\Component\DependencyInjection\ContainerInterface;

class LazyEventHandlers {
    /**
     * @var ContainerInterface
     */
    private $container;

    private $map = [];

    private $alreadyLoaded = [];

    public function __construct(
        ContainerInterface $container
    ) {
        $this->container = $container;
    }

    public function onBeforeEvent($eventName) {
        if ( !array_key_exists($eventName, $this->map) ) {
            return;
        }

        if ( in_array($eventName, $this->alreadyLoaded) ) {
        	return;
        }

        foreach ($this->map[$eventName] as $serviceId) {
            $this->container->get($serviceId);
        }

        $this->alreadyLoaded[] = $eventName;
    }

    public function addMapping(string $eventName, string $serviceId) {
        if ( !array_key_exists($eventName, $this->map) ) {
            $this->map[$eventName] = [];
        }
        $this->map[$eventName][] = $serviceId;
    }

    public function getMappedServiceIds(string $eventName): array {
	    if ( !array_key_exists($eventName, $this->map) ) {
	    	return [];
	    }

	    return $this->map[$eventName];
    }
}