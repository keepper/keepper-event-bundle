<?php
namespace Keepper\KeepperEventBundle;

use Keepper\ConventionSkeleton\SkeletonBundle;
use Keepper\KeepperEventBundle\DependencyInjection\EventCompilerPass;
use Symfony\Component\DependencyInjection\ContainerBuilder;

class KeepperEventBundle extends SkeletonBundle {

	public function build(ContainerBuilder $container) {
		$container->addCompilerPass(new EventCompilerPass());
	}
}