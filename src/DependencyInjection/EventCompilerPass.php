<?php
namespace Keepper\KeepperEventBundle\DependencyInjection;

use Keepper\KeepperEventBundle\Services\LazyEventHandlers;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\Definition;
use Symfony\Component\DependencyInjection\Reference;

class EventCompilerPass implements CompilerPassInterface {

	const LazyHandlersServiceId = 'KeepperEventBundle.LazyBinder';
    const MainTag = 'keepper-event-subscriber';
    const HandlerTag = 'keepper-event-handler';

    public function process(ContainerBuilder $container) {
        $definition = new Definition(LazyEventHandlers::class);
        $definition->addArgument(new Reference('service_container'));
        $container->setDefinition(self::LazyHandlersServiceId, $definition);

        $subscribersServices = $container->findTaggedServiceIds(self::MainTag);

        foreach ($subscribersServices as $id => $tags) {
            $container->getDefinition($id)->addMethodCall(
                'addListener',
                ['beforeDispathEvent', [new Reference(self::LazyHandlersServiceId), 'onBeforeEvent']]
            );
        }

        $handlersServices = $container->findTaggedServiceIds(self::HandlerTag);

        foreach ($handlersServices as $id => $tags) {
            $definition->addMethodCall('addMapping', [$tags[0]['eventName'], $id]);
        }
    }
}