# KeepperEventBundle

Add support event from keepper/lib-evens in symfony

## Usage

1. Create handler service, like:

        class HandlerService {
            public function __construct(EventSubscriberInterface $subscriber) {
                $subscriber->addListener('someEventName', [$this, 'onEvent']);
            }
            
            public function onEvent(int $myEventArg) {
            }
        }
        
2. Configure Subscriber

        services:
            ...
            MyEventSubscriber:
                class: Keepper\Lib\Events\EventSubscriber
                public: true
                tags:
                      - 'keepper-event-subscriber'
                      
3. Configure Handler

        services:
            ...
            MyEventHandler:
                class: HandlerService
                public: true
                tags:
                      - {name: 'keepper-event-handler', eventName: 'someEventName'}
              

## Installation

1. Add to repositories property in your composer.json

        "repositories": [
            ...
            {
              "type": "vcs",
              "url": "https://gitlab.com/keepper/convention-skeleton-bundle.git"
            },
            {
               "type": "vcs",
               "url": "https://gitlab.com/keepper/keepper-event-bundle.git"
            },
            {
              "type": "vcs",
              "url": "https://gitlab.com/keepper/lib-events.git"
            }
        ]
        
2. Then run command

        composer require keepper/keepper-event-bundle