<?php
namespace Keepper\KeepperEventBundle\Tests\Services;

use Keepper\KeepperEventBundle\Services\LazyEventHandlers;
use Symfony\Component\DependencyInjection\ContainerInterface;

class LazyEventHandlersTest extends \PHPUnit_Framework_TestCase {

	/**
	 * @var LazyEventHandlers
	 */
	private $service;

	/**
	 * @var \PHPUnit_Framework_MockObject_MockObject|ContainerInterface
	 */
	private $container;

	public function setUp() {
		parent::setUp();
		$this->container = $this->getMockBuilder(ContainerInterface::class)->getMock();
		$this->service = new LazyEventHandlers($this->container);
	}

	public function testGetServiceWithoutMap() {
		$this->container->expects($this->never())->method('get');
		$this->service->onBeforeEvent('some-event-name');
	}

	public function testGetServiceWithMap() {
		$eventName      = 'some-event-name';
		$serviceName    = 'some-service-id';

		$this->service->addMapping($eventName, $serviceName);
		$this->container
			->expects($this->once())
			->method('get')
			->with($serviceName);
		$this->service->onBeforeEvent($eventName);
	}
}