<?php
namespace Keepper\KeepperEventBundle\Tests;

use Keepper\Lib\Events\Interfaces\EventSubscriberInterface;

class TestEventHandler {
	public $handled = 0;

	const EVENT = 'test-event-name';

	public function __construct(
		EventSubscriberInterface $subscriber
	) {
		$subscriber->addListener(self::EVENT, [$this, 'eventHandler']);
	}

	public function eventHandler() {
		$this->handled++;
	}
}