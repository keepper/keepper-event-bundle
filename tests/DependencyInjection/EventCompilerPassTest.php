<?php
namespace Keepper\KeepperEventBundle\Tests\DependencyInjection;

use Keepper\ConventionSkeleton\Tests\Symfony\ServiceTestCase;
use Keepper\KeepperEventBundle\DependencyInjection\EventCompilerPass;
use Keepper\KeepperEventBundle\KeepperEventBundle;
use Keepper\KeepperEventBundle\Tests\TestEventHandler;
use Keepper\Lib\Events\EventDispatcher;
use Keepper\Lib\Events\EventSubscriber;
use Keepper\Lib\Events\Interfaces\EventDispatcherInterface;
use Keepper\Lib\Events\Interfaces\EventSubscriberInterface;
use Psr\Log\NullLogger;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Definition;

class EventCompilerPassTest extends ServiceTestCase {

	function bundleClassesToRegister(array $addTo = []): array {
		return parent::bundleClassesToRegister([
			KeepperEventBundle::class
		]);
	}

	private function process(ContainerBuilder $cb) {
		(new EventCompilerPass())->process($cb);
	}

	public function testAddDefinitionOfLazyEventHandlersService() {
		$cb = new ContainerBuilder();
		$this->process($cb);

		// Ожидаем, что добавилось определение сервиса
		$cb->findDefinition(EventCompilerPass::LazyHandlersServiceId);
	}

	public function testBindOnBeforeEventOnSubscriberServices() {
		$cb = new ContainerBuilder();

		// Добавляем сервис subscriber и отмечаем его тегом
		$subscriberDef = new Definition(EventSubscriber::class);
		$subscriberDef->addTag(EventCompilerPass::MainTag);
		$cb->setDefinition('SomeSubscriber', $subscriberDef);

		$this->process($cb);

		// Проверяем, что мы подписались на событие beforeEvent
		/**
		 * @var EventSubscriberInterface $subscriber
		 */
		$subscriber = $cb->get('SomeSubscriber');
		$this->assertTrue($subscriber->hasListeners(EventDispatcher::BEFORE_DISPATCH));
		$lazyService = $cb->get(EventCompilerPass::LazyHandlersServiceId);
		$handlers = $subscriber->getListeners(EventDispatcher::BEFORE_DISPATCH);

		$this->assertCount(1, $handlers);
		$this->assertEquals($lazyService, $handlers->current()[0]);
	}

	public function testAddMappingOnEventHandlers() {
		$cb = new ContainerBuilder();

		// Добавляем сервис subscriber и отмечаем его тегом
		$subscriberDef = new Definition(EventSubscriber::class);
		$subscriberDef->addTag(EventCompilerPass::MainTag);
		$cb->setDefinition('SomeSubscriber', $subscriberDef);

		// Добавляем сервис подписчик и отмечаем его тегом
		$handlerServiceDef = new Definition(NullLogger::class);
		$handlerServiceDef->addTag(EventCompilerPass::HandlerTag, ['eventName' => 'SomeEventName']);
		$cb->setDefinition('Somehandler', $handlerServiceDef);

		$this->process($cb);

		// Проверяем что добавился мапинг
		$lazyHandler = $cb->get(EventCompilerPass::LazyHandlersServiceId);
		$this->assertEquals(['Somehandler'], $lazyHandler->getMappedServiceIds('SomeEventName'));
	}

	public function testReal() {
		$handler = $this->getService('TestEventHandler');
		$this->assertEquals(0, $handler->handled);

		/**
		 * @var EventDispatcherInterface $dispatcher
		 */
		$dispatcher = $this->getService('TestEventDispatcher');

		$dispatcher->dispatch(TestEventHandler::EVENT);

		$this->assertEquals(1, $handler->handled);
	}
}